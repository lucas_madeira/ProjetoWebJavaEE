package br.com.byter.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;

import br.com.byter.qualificadores.Brasil;

public class FormatadorData {

	@Produces @Brasil
	public DateFormat getFormatadorDataBrasil(){
		return new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("pt", "BR"));
	}
	
	@Produces @Default
	public DateFormat getFormatadorDataUsa(){
		return new SimpleDateFormat("MM/dd/yyyy" ,Locale.US);
	}
}
