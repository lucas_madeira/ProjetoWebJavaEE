package br.com.byter.model;

public interface Mensageiro {
	public void enviarMensagem(String mensagem);
}
