package br.com.byter.controller;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.byter.model.Mensageiro;
import br.com.byter.qualificadores.MensageiroSMS;

@Named("enviarMensagemBean")
@RequestScoped
public class EnvioMensagemBean {

	@Inject @MensageiroSMS //@Default
	private Mensageiro mensageiro;
	
	private String texto;
	
	public void enviarMensagem(){
		mensageiro.enviarMensagem(texto);
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}	
}
