package br.com.byter.controller;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.byter.service.CalculadoraPreco;

@Named("meuBean")
public class PrecoProdutoBean {

	@Inject
	private CalculadoraPreco calculadora;
	
	public double getPreco(){
		return calculadora.calcularPreco(12,44.55);
	}
	
}
