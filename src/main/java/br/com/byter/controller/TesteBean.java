package br.com.byter.controller;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.byter.exception.NegocioException;
import br.com.byter.model.Mensageiro;
import br.com.byter.qualificadores.MensageiroSMS;

@Named("testeBean")
@RequestScoped
public class TesteBean {
	public void salvar(){
		throw new NegocioException("Mensagem"); 
	}
	
	public void salvar2(){
		throw new RuntimeException("Mensagem"); 
	}
}
