package br.com.byter.controller;

import java.text.DateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;

import br.com.byter.qualificadores.Brasil;

@Named
@javax.faces.view.ViewScoped
public class FormatacaoBean {

	@Inject@Brasil
	private DateFormat formatadorData;
	
	private Date dataInformada;
	private String dataFormatada;
	
	public void enviar(){
		dataFormatada = formatadorData.format(dataInformada);
	}

	public Date getDataInformada() {
		return dataInformada;
	}

	public void setDataInformada(Date dataInformada) {
		this.dataInformada = dataInformada;
	}

	public String getDataFormatada() {
		return dataFormatada;
	}

	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}	
}
